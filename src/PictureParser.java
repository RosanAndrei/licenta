import java.io.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

public class PictureParser
{

    public ProcessedMap ParsePicture(String path) throws IOException
    {
    	File file= new File(path);
        BufferedImage image = ImageIO.read(file);
        // Getting pixel color by position x and y

        int width = image.getWidth();//y
        int height = image.getHeight();//x
        int result = 0;
        
        
        ProcessedMap pm = new ProcessedMap(height,width);

        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                result = image.getRGB(col, row);
                switch (result)
                {
                    case -65536:
                    {
                        pm.Red.add(new Point(2,row,col));
                        pm.harta[row][col]= new Point(2, row, col);
                        break;
                    }
                    case -4621737:
                    {
                    	pm.Brown.add(new Point(1,row,col));
                    	pm.harta[row][col]= new Point(1, row, col);
                        break;
                    }
                    case -16776961:
                    {
                        pm.Blue.add(new Point(0,row,col));
                        pm.harta[row][col]= new Point(0, row, col);
                        break;
                    }
                    default:
                    {
                    	pm.harta[row][col]= new Point(3, row, col);
                        break;
                    }
                }

            }
        }
        return pm;
    }
}