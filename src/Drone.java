import java.util.LinkedList;

public class Drone extends Thread {
	private int droneNr, firstLastPointIndex = Integer.MAX_VALUE;
	private boolean removeFirstCoords = true;
	private Point start, actualPos, nextPos;
	private LinkedList<Point> toBeVisited;
	private int contor = 0;
	private DroneCommunicationModule dcm;
	
	//private DBInterface dbInterface;

	// constructor fara ruta
	public Drone(int number, Point bluePoint, DroneCommunicationModule module) {
		this.droneNr = number;
		this.start=bluePoint;
		this.actualPos = bluePoint;
		this.dcm = module;
		this.toBeVisited = new LinkedList<Point>();
		
	}

	// contructor cu ruta
	/*public Drone(int number, Point bluePoint, LinkedList<Point> route) {
		this.droneNr = number;
		this.toBeVisited = route;
		this.start=bluePoint;
		this.actualPos = bluePoint;
		this.nextPos = route.get(0);
		
	}*/

	public void run() {
		while (contor < 2) {

			moveNext();
			confirmVisited(this.getDroneNr(), this.getPosition());
			//dbInterface.updatePointStatus("Vizitat", this.getDrone(), this.actualPos);
			getNextPos();
			//dbInterface.insertCoord(getDrone(), this.actualPos.getX(), this.actualPos.getY());
			removeInitialPoint();

		}
		System.out.println("Drone "+this.getDroneNr()+" finished cycle: "+actualPositionToString());

	}

	public int getDroneNr() {
		return this.droneNr;
	}
	
	public Point getPosition()
	{
		return this.actualPos;
	}
	
	public Point getStartPosition()
	{
		return this.start;
	}

	public void setRoute(LinkedList<Point> route, boolean overrideCurrentRoute) {
		/*try {
			this.wait(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}*/
		if(overrideCurrentRoute)
		{
			route.forEach(point-> {
				this.toBeVisited.add(point);
			});
		}
		else {
			this.toBeVisited.clear();
			this.toBeVisited = route;
			this.nextPos = route.get(0);
		}
	}

	public void moveNext() {

		System.out.println(
				"Drone nr. " + this.droneNr + ": Actual position:" + this.actualPos + " - nextPos:" + this.nextPos);
		while (!this.actualPos.equals(this.nextPos)) {
			this.moveNextCoords();
			this.dcm.sendPosition(this.getDroneNr(), this.getPosition());

			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void moveNextCoords() {
		int actualX = actualPos.getX();
		int actualY = actualPos.getY();
		int nextX = nextPos.getX();
		int nextY = nextPos.getY();

		if (actualX < nextX && actualY < nextY) {
			actualPos.setX(actualX + 1);
			actualPos.setY(actualY + 1);
		} else if (actualX < nextX && actualY > nextY) {
			actualPos.setX(actualX + 1);
			actualPos.setY(actualY - 1);
		} else if (actualX < nextX && actualY == nextY) {
			actualPos.setX(actualX + 1);
		} else if (actualX > nextX && actualY < nextY) {
			actualPos.setX(actualX - 1);
			actualPos.setY(actualY + 1);
		} else if (actualX > nextX && actualY > nextY) {
			actualPos.setX(actualX - 1);
			actualPos.setY(actualY - 1);
		} else if (actualX > nextX && actualY == nextY) {
			actualPos.setX(actualX - 1);
		} else if (actualX == nextX && actualY < nextY) {
			actualPos.setY(actualY + 1);
		} else if (actualX == nextX && actualY > nextY) {
			actualPos.setY(actualY - 1);
		}
	}

	private void getNextPos() {
		for (int i = 0; i < this.toBeVisited.size(); i++) {
			if (this.nextPos == this.toBeVisited.get(i)) {
				if (this.toBeVisited.get(i).equals(this.toBeVisited.getLast()) && i < firstLastPointIndex) {
					firstLastPointIndex = i;
				}
				if (this.nextPos == this.toBeVisited.getLast()) {
					this.nextPos = this.toBeVisited.getFirst();
					this.contor++;
					break;
				} else {
					this.nextPos = this.toBeVisited.get(i + 1);
					break;
				}
			}
		}
	}

	private void removeInitialPoint() {
		if (firstLastPointIndex < Integer.MAX_VALUE && removeFirstCoords) {
			for (int i = 0; i <= firstLastPointIndex; i++) {
				toBeVisited.removeFirst();
			}
			removeFirstCoords = false;
		}
	}
	
	public void confirmVisited(int droneNr, Point visited)
	{
		this.dcm.confirmVisited(droneNr, visited);
	}

	public String actualPositionToString() {
		return "Actual position: " + actualPos;
	}

	public String toString() {
		return "Actual position:" + this.actualPos + ", next position:" + this.nextPos + ", route:" + this.toBeVisited;
	}

}
