
public class Main {
	static Drone[] drones ;
	
	public static void main(String[] args)
	{
		drones = new Drone[20];
		CentralUnit cu = new CentralUnit();
		cu.beginProcessing();
		for (Drone d : drones) {
			if(d != null)
			{
				try {
					d.start();
				} catch (Exception e) {
					System.out.println("Error starting drone!");
				}
			}
		}
		
	}
}
