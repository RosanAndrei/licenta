import java.util.LinkedList;

public class CommunicationModule {
	private DroneCommunicationModule[] dcm = new DroneCommunicationModule[20]; 
	private DBInterface dbInterface = new DBInterface();
	
	public void connectToDrone(Point bluePoint) {
		for (int i=0; i < dcm.length; i++) {
			if(!isConnectedToDrone(dcm[i]))
			{
				dcm[i] = new DroneCommunicationModule(i+1,bluePoint, this);
				System.out.println("Connected to drone no."+(i+1));
				break;
			}
			if(i == dcm.length-1)
			{
				System.out.println("No drone available!");
			}
		}
	}

	public boolean isConnectedToDrone(DroneCommunicationModule module) {
		if(module==null)
		{
			return false;
		}
		else {
			return true;
		}
	}

	public void sendRouteToDrone(Point bluePoint, LinkedList<Point> route) {
		for(int i = 0; i < dcm.length; i++)
		{
			if(isConnectedToDrone(dcm[i]))
			{
				if(dcm[i].getStartPosition().equals(bluePoint))
				{
					dcm[i].receiveRoute(route, false);
					 {
						this.dbInterface.insertRoute(dcm[i].getDroneNr(), route);
					}
					
				}
			}
		}
	}

	public void receivePosition(int droneNr, Point position) {
		{
			this.dbInterface.insertCoord(droneNr, position);
		}
		
	}
	
	public void receiveVisited(int droneNr, Point visited)
	{
		{
			this.dbInterface.updatePointStatus("Vizitat", droneNr, visited);
		}
		
	}

}