import java.util.ArrayList;
import java.util.LinkedList;

public class CentralUnit {

	public void beginProcessing() {
		PictureParser pp = new PictureParser();
		//ArrayList<Drone> droneList = new ArrayList<Drone>();
		RouteCalculator rc = new RouteCalculator();
		CommunicationModule cm = new CommunicationModule();
		//DBInterface dbInterface = new DBInterface();
		
		try {
			ProcessedMap procMap = pp.ParsePicture("obstacole.png");
			procMap.Blue.forEach(bluePoint->{
				cm.connectToDrone(bluePoint);
			});
			ArrayList<LinkedList<Point>> rute = rc.CalculateRoutes(procMap, procMap.Blue);
			// for each route, addFirst(dronePos)
			for(int i = 0; i < rute.size(); i++)
			{
				cm.sendRouteToDrone(procMap.Blue.get(i), rute.get(i));
			}
			
			
			//afisare fiecare punct din fiecare ruta
			//rute.forEach(lList -> lList.forEach(punct -> System.out.println(punct.toString())));

			/*for (int i = 0; i < rute.size(); i++) {
				droneList.add(new Drone(i + 1, procMap.Blue.get(i)));

				if (!cm.isConnectedToDrone(droneList.get(i))) {
					cm.connectToDrone();

				}
				cm.sendRouteToDrone(droneList.get(i), rute.get(i));
				 
			}*/

			//dbInterface.insertRute(droneList);
			//droneList.forEach(drone -> drone.start());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
