import java.util.LinkedList;

public class DroneCommunicationModule {
	private Drone drona;
	private CommunicationModule cm;
	
	public DroneCommunicationModule(int droneNr ,Point bluePoint, CommunicationModule module) {
		this.drona = new Drone(droneNr, bluePoint, this);
		Main.drones[droneNr] = this.drona;
		this.cm = module;
	}
	
	public int getDroneNr()
	{
		return this.drona.getDroneNr();
	}
	
	public Point getPosition()
	{
		return this.drona.getPosition();
	}
	
	public Point getStartPosition()
	{
		return this.drona.getStartPosition();
	}
	
	public void receiveRoute(LinkedList<Point> route, boolean overrideCurrentRoute)
	{
		this.drona.setRoute(route, overrideCurrentRoute);
	}
	
	public void confirmVisited(int droneNr, Point visited)
	{
		this.cm.receiveVisited(droneNr, visited);
	}
	
	public void sendPosition(int droneNr, Point position)
	{
		this.cm.receivePosition(droneNr, position);
	}

}
