
public class Point {

	private int x;
	private int y;
	private int cellType = 0;
	private int hops;
	private int lastX;
	private int lastY;
	private double dToEnd = 0;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Point(int type, int x, int y) {
		cellType = type;
		this.x = x;
		this.y = y;
		hops = -1;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getLastX() {
		return lastX;
	}

	public int getLastY() {
		return lastY;
	}

	public int getType() {
		return cellType;
	}

	public int getHops() {
		return hops;
	}

	public void setX(int X) {
		this.x = X;
	}

	public void setY(int Y) {
		this.y = Y;
	}

	public void setType(int type) {
		cellType = type;
	}

	public void setLastNode(int x, int y) {
		lastX = x;
		lastY = y;
	}

	public void setHops(int hops) {
		this.hops = hops;
	}

	public double distanta(Point p2) {
		return Math.sqrt(Math.pow((p2.x - this.x), 2) + Math.pow((p2.y - this.y), 2));
	}

	public double getEuclidDist(Point target) {
		int xdif = Math.abs(this.x - target.x);
		int ydif = Math.abs(this.y - target.y);
		dToEnd = Math.sqrt((xdif * xdif) + (ydif * ydif));
		return dToEnd;
	}

	public boolean equals(Object p) {
		Point pp = (Point) p;

		if (pp.x == this.x && pp.y == this.y) {
			return true;
		} else {
			return false;
		}
	}

	public String toString() {
		return "[x:" + this.x + ", y:" + this.y + "]";
	}
}
