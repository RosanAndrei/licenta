import java.sql.Connection;
import java.sql.DriverManager;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class DBInterface {
	private String dbURL = "jdbc:sqlserver://localhost:54820;databaseName=Licenta";// port=54820
	private String user = "Licenta";
	private String pass = "Parola";
	private Connection conn = null;

	public synchronized void insertCoord(int droneNr, Point position) {
		try {

			conn = DriverManager.getConnection(dbURL, user, pass);
			if (conn != null) {
				String sql = "INSERT INTO Coordonate VALUES(" + droneNr + "," + position.getX() + "," + position.getY() + ", GETDATE())";
				Statement statement = conn.createStatement();
				statement.executeUpdate(sql);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

	public synchronized void insertRoute(int droneNr, LinkedList<Point> route) {
		try {
			conn = DriverManager.getConnection(dbURL, user, pass);
			if (conn != null) {
				route.forEach(point-> {
						String sql = "INSERT INTO Rute VALUES(" + droneNr + "," + point.getX() + ","
								+ point.getY() + ", '�n curs de a fi vizitat')";
						Statement statement;
						try {
							statement = conn.createStatement();
							statement.executeUpdate(sql);
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					});
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

	public synchronized  void updatePointStatus(String status, int droneNr, Point position) {
		try {
			conn = DriverManager.getConnection(dbURL, user, pass);
			if (conn != null) {
				String sql = "UPDATE Rute SET Status='" + status + "' WHERE Drona=" + droneNr + " AND X="
						+ position.getX() + " AND Y=" + position.getY();
				Statement statement = conn.createStatement();
				statement.executeUpdate(sql);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}

}
