
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;

public class RouteCalculator {

	private Point startPoint, lastPoint;
	private LinkedList<Point> puncteDeVizitat;
	private Point[][] tempMap;
	private int dimensiuneX, dimensiuneY;
	private ArrayList<Point> traseu;
	private ArrayList<Point> vizitate;

	public boolean solving = false;

	public ArrayList<LinkedList<Point>> CalculateRoutes(ProcessedMap pm, List<Point> coord) {
		ArrayList<LinkedList<Point>> trasee = new ArrayList<LinkedList<Point>>();
		traseu = new ArrayList<Point>();
		vizitate = new ArrayList<Point>();
		dimensiuneX = pm.harta.length;
		dimensiuneY = pm.harta[0].length;
		ArrayList<LinkedList<Point>> routes = new ArrayList<LinkedList<Point>>();

		for (int i = 0; i < coord.size(); i++) {
			routes.add(new LinkedList<Point>());
		}

		// se impart punctele maro intre drone folosind algoritm Greedy
		getGreedyPoints(pm, coord, routes);

		// aici se foloseste A* pentru a determina calea punct cu punct catre punctele
		// maro impartite pe drone
		getDetailedRoutes(trasee, coord, routes, pm);

		return trasee;
	}

	private void getGreedyPoints(ProcessedMap pm, List<Point> coord, ArrayList<LinkedList<Point>> routes) {
		LinkedList<Point> tbv = pm.Brown;
		while (tbv.size() > 0) {
			for (int i = 0; i < coord.size(); i++) {
				if (tbv.size() > 0) {
					Point currentPoint;
					if (routes.get(i).size() == 0) {
						currentPoint = coord.get(i);
					} else {
						currentPoint = routes.get(i).getLast();
					}

					Point greedyNext = getNext(currentPoint, tbv);
					routes.get(i).add(greedyNext);
					tbv.remove(greedyNext);
				}
			}
		}
	}

	private void getDetailedRoutes(ArrayList<LinkedList<Point>> trasee, List<Point> coord,
			ArrayList<LinkedList<Point>> routes, ProcessedMap pm) {
		for (int i = 0; i < coord.size(); i++) {
			trasee.add(new LinkedList<Point>());
			startPoint = coord.get(i);
			puncteDeVizitat = routes.get(i);
			if (routes.get(i).size() > 1) {
				puncteDeVizitat.add(puncteDeVizitat.getFirst());
			}
			for (int j = 0; j < puncteDeVizitat.size(); j++) {
				lastPoint = puncteDeVizitat.get(j);
				tempMap = pm.harta;
				solving = true;
				AStar(tempMap, startPoint, lastPoint);
				startPoint = lastPoint;
				for (int c = traseu.size() - 1; c >= 0; c--) {
					trasee.get(i).add(new Point(traseu.get(c).getX(), traseu.get(c).getY()) );
				}
				vizitate.forEach(vizitat -> {
					vizitat.setHops(-1);
					vizitat.setLastNode(0, 0);
					vizitat.setType(3);
				});
				vizitate.clear();
				traseu.clear();
			}
		}
	}

	private static Point getNext(Point cp, LinkedList<Point> toBeVisited) {
		Point next = toBeVisited.get(0);
		double minDist = Double.MAX_VALUE;
		double dist;
		for (int i = 0; i < toBeVisited.size(); i++) {
			dist = cp.distanta(toBeVisited.get(i));
			if (dist < minDist) {
				next = toBeVisited.get(i);
				minDist = dist;
			}
		}

		return next;
	}

	public void AStar(Point[][] map, Point first, Point last) {
		ArrayList<Point> priority = new ArrayList<Point>();
		priority.add(map[first.getX()][first.getY()]);
		while (this.solving) {
			if (priority.size() <= 0) {
				this.solving = false;
				break;
			}
			int hops = priority.get(0).getHops() + 1;
			ArrayList<Point> explored = exploreNeighbors(priority.get(0), hops);
			if (explored.size() > 0) {
				priority.remove(0);
				priority.addAll(explored);
			} else {
				priority.remove(0);
			}
			sortQue(priority);
		}
	}

	public ArrayList<Point> sortQue(ArrayList<Point> sort) {
		int c = 0;
		while (c < sort.size()) {
			int sm = c;
			for (int i = c + 1; i < sort.size(); i++) {
				if (sort.get(i).getEuclidDist(lastPoint) + sort.get(i).getHops() < sort.get(sm).getEuclidDist(lastPoint)
						+ sort.get(sm).getHops())
					sm = i;
			}
			if (c != sm) {
				Point temp = sort.get(c);
				sort.set(c, sort.get(sm));
				sort.set(sm, temp);
			}
			c++;
		}
		return sort;
	}

	public ArrayList<Point> exploreNeighbors(Point current, int hops) {
		ArrayList<Point> explored = new ArrayList<Point>();
		for (int a = -1; a <= 1; a++) {
			for (int b = -1; b <= 1; b++) {
				int xbound = current.getX() + a;
				int ybound = current.getY() + b;
				if ((xbound > -1 && xbound < dimensiuneX) && (ybound > -1 && ybound < dimensiuneY)) {
					Point neighbor = tempMap[xbound][ybound];
					if ((neighbor.getHops() == -1 || neighbor.getHops() > hops) && neighbor.getType() != 2) {
						explore(neighbor, current.getX(), current.getY(), hops);
						explored.add(neighbor);
						vizitate.add(neighbor);
					}
				}
			}
		}
		return explored;
	}

	public void explore(Point current, int lastx, int lasty, int hops) {
		current.setLastNode(lastx, lasty);
		current.setHops(hops);
		if (current.equals(lastPoint)) {
			traseu.add(current);
			backtrack(current.getLastX(), current.getLastY(), hops);
		}
	}

	public void backtrack(int lx, int ly, int hops) {

		while (hops > 1) {
			Point current = tempMap[lx][ly];
			traseu.add(current);
			lx = current.getLastX();
			ly = current.getLastY();
			hops--;
			if (hops == 1) {
				traseu.add(tempMap[lx][ly]);
			}
		}
		solving = false;
	}

}
